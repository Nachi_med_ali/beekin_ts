# Beekin Indeed Scraping + API

The repo contains code for Beekin assignment (indeed scraper + API jobs Endpoint)

## Prerequisites
- Node.js 10+
- Yarn or NPM

## Installation
- Install dependencies
```bash
yarn install
```

- Create application configuration
```bash
cp .env.example .env
nano .env
```

- Scrape job posts from indeed
```bash
yarn scrape
```

- Start Application
```bash
yarn start
```
Browse `http://localhost:4500/jobs` to view jobs retrieved by the scraper.

The application will be launched by [Nodemon](https://nodemon.com). It will restart automatically on file change
