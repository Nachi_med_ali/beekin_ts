import express from 'express';
import dotenv from 'dotenv';
import { connectToDatabase } from './databaseConnection';
import { Job } from './models/job';

dotenv.config();

const HOST = process.env.HOST || 'http://localhost';
const PORT = parseInt(process.env.PORT || '4500');

const app = express();

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

/* including swagger UI for API doc*/
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('../swagger.json');
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

/* Defining allowed filtering fields for Job*/
const FILTER_KEYS = ["title", "location", "description", "company"]

app.get('/jobs', async (req, res) => {

  const filters = req.query;

  var jobs = await Job.find().sort({ title: 1 });

  /* if filtering is set*/
  if (Object.keys(filters).length !== 0){
    jobs = await jobs.filter(job => {
      let isValid = true;
      let key : string = "";
      for (key in filters) {
        if (FILTER_KEYS.includes(key)){
          const filter_value : string = filters[key] as string || "";
          isValid = isValid && job[key].toLowerCase().includes(filter_value.toLowerCase());
        }
      }
      return isValid;
    });

  } 

  return res.json({ data: jobs});
});

app.listen(PORT, async () => {
  await connectToDatabase();
  console.log(`Application started on URL ${HOST}:${PORT}`);
});
