import mongoose, { Model, Schema, Document } from 'mongoose';

type JobDocument = Document & {
  title: string;
  location : string;
  description : string;
  tech : string[];
  experience : string;
  created_at : Date;
  tasks : string[];
  company : string;
  indeed_link : string;
};

const JobSchema = new Schema(
  {
    title: {
      type: Schema.Types.String,
      required: true,
      index: true,
    },
    location: {
      type: Schema.Types.String,
    },
    description: {
      type: Schema.Types.String,
    },
    tech: {
      type: [Schema.Types.String],
    },
    experience: {
      type: Schema.Types.Number,
    },
    created_at: {
      type: Schema.Types.String,
    },
    tasks: {
      type: [Schema.Types.String],
    },
    company: {
      type: Schema.Types.String,
    },
    indeed_link: {
      type: Schema.Types.String,
    },
  },
  {
    collection: 'jobs',
    timestamps: true,
  },
);

const Job: Model<JobDocument> = mongoose.model<JobDocument>('Job', JobSchema);

export { Job, JobDocument };
