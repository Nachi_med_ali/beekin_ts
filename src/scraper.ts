import axios from 'axios';
import * as cheerio from 'cheerio';
import { Job } from './models/job';
import { connectToDatabase } from './databaseConnection';

/*const PAGE_URL = 'http://indeed.fr/jobs';*/

/*const PAGE_URL = "http://indeed.fr/jobs?q=python&l=paris&r=25&limit=2";
const BASE_URL = "http://indeed.fr/";*/


type JobPost = {
  title: string;
  location : string;
  description : string;
  tech : string[];
  experience? : number;
  created_at? : Date;
  tasks : string[];
  company : string;
  indeed_link : string;
};

type OptionObj = {
  q: string;
  location : string;
  language : string;
  radius : number;
  limit : number;
};

let BASE_URLS = {
    "fr" : "https://indeed.fr/",
    "en" : "https://indeed.com/",
    "uk" : "https://uk.indeed.com/",
    "de" : "https://de.indeed.com/"
}

const getArgs = () =>{

    const args = process.argv.slice(2);
    let params = {};

    args.forEach(a => {
        const nameValue = a.split("=");
        params[nameValue[0]] = nameValue[1];
    });
    return params;
}

const _buildQueryString = (options : OptionObj) => {

    /*Validate option Obj*/
    if(options === null) {
        throw '_buildQueryString() - `options` parameter is null.'; return;
    } else if(options.q === null || options.q.length < 1) {
        throw '_buildQueryString() - `options.q` parameter is required.'; return;
    } else if(options.location === null || options.location.length < 1) {
        throw '_buildQueryString() - `options.location` parameter is required.'; return;
    } else if(options.language === null) {
        throw '_buildQueryString() - `options.language` parameter is required.'; return;
    } else if(options.language !== null && !(options.language in BASE_URLS)) {
        throw '_buildQueryString() - `options.language` needs to be valid (fr, en, uk, de).'; return;
    }

    let queryString : string = BASE_URLS[options.language]+"jobs",
        optionProperties = Object.keys(options)
    ;

    // Regular expresson to detect any whitespace character
    let ws_regex = /\s+/g;

    // For each property in the object, if it's defined then add to `queryString`.
    optionProperties.forEach(function(property) {

        switch(property) {
            case 'q': queryString += '?q=' + options[property].toString().replace(ws_regex, '+'); break;
            case 'location': queryString += '&l='   + options[property].toString().replace(ws_regex, '+'); break;
            case 'radius': queryString += '&r='     + options[property].toString().replace(ws_regex, '+'); break;
            case 'limit': queryString += '&limit='  + options[property].toString().replace(ws_regex, '+'); break;
            case 'language': break;

            default: console.log(`[!] Warning: Property [${property}] not supported.`);
        }
    });

    return queryString;
};

/* Convert days to ML secs */
const daystoMLsecs = (nbr_days : number) => {
    return nbr_days * 24 * 60 * 60 * 1000
}

/* Retrieve publication date based on Nbr of days publication text*/
const retrievePublicationDate = (content : string) => {
    const nbr_days : number = Number(content.match(/\d+/)) || 30;
    let dateTimee = new Date();
    if (nbr_days && nbr_days < 30 ){
        let dateTime = new Date().getTime();
        let newDateObj = new Date(dateTime - daystoMLsecs(nbr_days));
        return newDateObj
    }
    return undefined;

}

/*Retieve experience years from description */
const retrieveExperience = (content : string) => {
    const nbr_years : number = Number(content.match(/(\d+)[^,.\d\n]+?(?=year|an)/)) || 0;
    return nbr_years
}

/*Retrieve all jobs urls from indeed search URL */
const retrieveURLS = (content : string , language : string) => {
    let links : string[] = [];
    const $ = cheerio.load(content);
    const jobs_links = $('a.result');

    for (let i = 0; i < jobs_links.length; i++) {
        const _url = jobs_links.eq(i);
        const uri : string = _url.attr('href') || "";
        let full_url = new URL(uri, BASE_URLS[language]);
        links.push(full_url.href);
    }
    return links
}


const retrieveJobData = (content : string, indeed_link : string ) => {
    const $ = cheerio.load(content);

    const description : string = $('div.jobsearch-JobComponent-description').text();
    const publication_date : string = $('span.jobsearch-HiringInsights-entry--text').text();

    const job: JobPost = {
        title: $('h1.jobsearch-JobInfoHeader-title').text(),
        location : $('div.jobsearch-JobInfoHeader-subtitle').children('div').eq(1).text(),
        description : description,
        tech : [],
        experience : retrieveExperience(description),
        created_at : retrievePublicationDate(publication_date),
        tasks : [],
        company : $('div.jobsearch-InlineCompanyRating').children('div').eq(1).text(),
        indeed_link : indeed_link
    };

    return job
}



const scraper = async () => {


/*
    Defining scraper parameters as arguments

    const args = getArgs();
    console.log(args["q"]);

    var q = process.argv[2] || "" ;
    var language = process.argv[3] || "";
    var location = process.argv[4] || "";
    var radius = process.argv[5] || 0;
    var limit = process.argv[6] || 0;
*/

    /* Reading the options for scraping*/
    const options = require('../config.json');

    /*Generating the search URL*/
    const BASE_URL = _buildQueryString(options) || "";

    /*Retrieving job URLS*/
    const response = await axios.get(BASE_URL);
    const job_links = retrieveURLS(response.data, options["language"]);

    /*Promise on each job_link to get different required data*/
    const jobs : JobPost[] = await Promise.all(job_links.map(async (job_link) => {
            const job_response = await axios.get(job_link);
            return retrieveJobData(job_response.data, job_link);
        })
    );

    /*Connecting to DB*/
    await connectToDatabase();

    /* Inserting Data to DB 
    Using the indeed link as a unique parameter to avoid duplicate jobs
    */
    const insertPromises = jobs.map(async (job) => {
        const isPresent = await Job.exists({ indeed_link: job.indeed_link });

        if (!isPresent) {
            await Job.create(job);
        }
    });

    await Promise.all(insertPromises);

    console.log('Data inserted successfully!');
};

(async () => {
  await scraper();
})();
